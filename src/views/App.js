import logo from "./logo.svg";
import "./App.scss";
// import MyComponent from "./Example/MyComponent";
import ListToDo from "./Todo/ListToDo.js";
import React from "react";
/**
 *
 * @returns
 */
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* <MyComponent></MyComponent>  */}
        <ListToDo />
      </header>
    </div>
  );
}

export default App;
