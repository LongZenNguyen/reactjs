import React  from 'react';

class AddComponent extends React.Component{
    state = {
        title: '',
        salary: ''
    }
    handleChangeTitleJob = (event) => {
        this.setState({
            title: event.target.value
        })
    }
    handleChangeSalary = (event) => {
        this.setState({
            salary : event.target.value
        })
    }
    handleOnclick = (event) => {
        if(!this.state.title || !this.state.salary){
            alert("Missing partasms")
            return 
        }
        event.preventDefault();
         console.log(">>> check input",this.state)
        this.props.addNewJob({
            id: Math.floor(Math.random() * 10001),
            title: this.state.title,
            salary: this.state.salary})
        this.setState({
            title: '',
            salary: ''
        })
    }

    render() {
        // console.log(">>> Check data: ",this.state)
        return(
            <form>
            <label htmlFor="fname">Job's Title: </label>
            <br/>
                <input tyope="text" value={this.state.title} onChange={ (event) => this.handleChangeTitleJob(event)}/>
            <br/>
            <label htmlFor="lname">Salary: </label>
            <br/>
                <input tyope="text" value={this.state.salary} onChange={ (event) => this.handleChangeSalary(event)}/>
            <br/><br/>
                <input type="submit"
                     onClick={(event) => this.handleOnclick(event)}
                />

        </form>
        )
    }
}

export default AddComponent;