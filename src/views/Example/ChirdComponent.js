import React from "react";
import "./Demo.scss";

class ChirdComponent extends React.Component {
  state = {
    isShow: false,
    arrJobs: this.props.arrJobs,
    reload: true,
  };

  handleShowData = () => {
    this.setState({
      isShow: !this.state.isShow,
    });
  };

  onHandleDelete = (event) => {
    this.props.handleOnClickDelete(event);
  };

  render() {
    console.log(">>> prods: ", this.props);
    console.log(">>> state: ", this.state.arrJobs);
    let { isShow } = this.state;
    let check = isShow === true ? "isShow = true" : "is Show = false";
    console.log(">>>> Check condition : ", check);
    return (
      <>
        {isShow === false ? (
          <>
            <div>
              <button
                className="btn-show"
                onClick={(event) => this.handleShowData()}
              >
                Show
              </button>
            </div>
          </>
        ) : (
          <>
            <div className="job-lists">
              {this.state.arrJobs.map((item, index) => {
                return (
                  <div key={item.id}>
                    {item.title} - {item.salary} <></>
                    <button
                      onClick={() => {
                        this.onHandleDelete(index);
                        this.setState({ reload: !this.state.reload });
                      }}
                    >
                      X
                    </button>
                  </div>
                );
              })}
            </div>
            <div>
              <button onClick={(event) => this.handleShowData()}>Hide</button>
            </div>
          </>
        )}
      </>
    );
  }
}

export default ChirdComponent;
