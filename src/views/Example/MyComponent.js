import React from 'react'
import ChirdComponent from './ChirdComponent'
import AddComponent from './AddComponent'
class MyComponent extends React.Component{
    state = {
        arrJobs: [
            {id: 'aJob1', title:'Deleloper',salary:'500'},
            {id: 'aJob2', title:'Tester',salary:'400'},
            {id: 'aJob3', title:'PM',salary:'1000'},
        ]
    }
    
    handleOnclick = (event) => {
        event.preventDefault();
         console.log(">>> check input",this.state)
    }

    addNewJob = (job) => {
        console.log(">>>> check jobv: ",job)
       this.setState({
          arrJobs:[...this.state.arrJobs,job]
       })
    }
    handleOnClickDelete = (event) => {
        console.log(">>> index: ",event)
        console.log(">>> arrJobs1: ",this.state.arrJobs)
        // 1 4 3 5 7  |  index : 2
       this.state.arrJobs.splice(event,1)

        console.log(">>> arrJobs2: ",this.state.arrJobs)
    }

    render() {
        return (
            <>
                <AddComponent
                    addNewJob={this.addNewJob}
                />
                <ChirdComponent 
                    name={this.state.first_name} 
                    age={'25'}
                    address={'Hai Phong'}
                    arrJobs={this.state.arrJobs}
                    handleOnClickDelete={this.handleOnClickDelete}
                />
            </>
        )
    }
}

export default MyComponent;