import React from 'react'
class ListToDo extends React.Component{
    state = {
        listTodos: [
            {id: 'todo1',title: 'doing homwork'},
            {id: 'todo2',title: 'Coding'},
            {id: 'todo3',title: 'Fixing bog'}
        ]
    }
    render(){
        // let {listTodos} = this.state;
        return (
            <div className='list-todo-container'>
                <div className='add-todo'>
                    <input type="text"/>
                    <button type="button">Add</button>
                </div>
                <div className='list-todo-content'>
                    <div className='todo-chird'>
                        <span>Todo1</span>
                        <></>
                        <button>Edit</button>
                        <button>Delete</button>
                    </div>
                    <div className='todo-chird'>
                        <span>Todo1</span>
                        <></>
                        <button>Edit</button>
                        <button>Delete</button>
                    </div>
                    <div className='todo-chird'>
                        <span>Todo1</span>
                        <></>
                        <button>Edit</button>
                        <button>Delete</button>
                    </div>
                </div>

            </div>
        )
    }
}
export default ListToDo;