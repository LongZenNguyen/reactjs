import React from "react";
import CurrencyTextField  from '@unicef/material-ui-currency-textfield';

class InputNumberCustom extends React.Component{
   render() {
      return (
         <CurrencyTextField
                    label="Input text"
                    variant="standard"
                    currencySymbol=""
                    outputFormat="string"
                    decimalCharacter="."
                    digitGroupSeparator=","/>
      )
  }
}
export default InputNumberCustom();